package org.example;

public class Registration {
    private String name;
    private int age;
    private double distance;
    public Registration(String name, int age, double distance) {
    this.name = name;
    this.age = age;
    this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", distance=" + distance +
                '}';
    }
}
