package org.example;

public class PriceSystem {
    public static final int PRICE_POR_KILOMETER = 35; // В гривнях

    public void priceCounter(Registration costumer) throws RegistrationException {
        double price = costumer.getDistance() * PRICE_POR_KILOMETER;
        if (costumer.getAge() <= 0) {
            throw new RegistrationException("Вам не може бути 0 років, будь ласка введіть дійсний вік");
        } else {
            System.out.println("Добрий день, " + costumer.getName() + " ціна за проїзд становить : " + price + " UAH");
        }
    }
}
